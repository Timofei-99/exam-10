import {
    DELETE_COMMENTS_FAILURE,
    DELETE_COMMENTS_REQUEST,
    DELETE_COMMENTS_SUCCESS,
    FETCH_COMMENTS_FAILURE,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS,
    POST_COMMENTS_FAILURE,
    POST_COMMENTS_REQUEST,
    POST_COMMENTS_SUCCESS,
} from "../actions/CommentActions";

const initialState = {
    error: false,
    loading: false,
    comments: [],
};

const CommentReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return { ...state, loading: true };
        case FETCH_COMMENTS_SUCCESS:
            return { ...state, loading: false, comments: action.comments };
        case FETCH_COMMENTS_FAILURE:
            return { ...state, loading: false, error: action.error };
        case DELETE_COMMENTS_REQUEST:
            return { ...state, loading: true };
        case DELETE_COMMENTS_SUCCESS:
            return { ...state, loading: false };
        case DELETE_COMMENTS_FAILURE:
            return { ...state, loading: false, error: action.error };
        case POST_COMMENTS_REQUEST:
            return { ...state, loading: true };
        case POST_COMMENTS_SUCCESS:
            return { ...state, loading: false };
        case POST_COMMENTS_FAILURE:
            return { ...state, loading: false, error: action.error };
        default:
            return state;
    }
};

export default CommentReducer;
