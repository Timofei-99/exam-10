import {
    DELETE_NEWS_FAILURE,
    DELETE_NEWS_REQUEST,
    DELETE_NEWS_SUCCESS,
    FETCH_NEWS_FAILURE,
    FETCH_NEWS_REQUEST,
    FETCH_NEWS_SUCCESS,
    FETCH_ONE_NEWS_FAILURE,
    FETCH_ONE_NEWS_REQUEST,
    FETCH_ONE_NEWS_SUCCESS,
    POST_NEWS_FAILURE,
    POST_NEWS_REQUEST,
    POST_NEWS_SUCCESS,
} from "../actions/NewsActions";

const initialState = {
    error: false,
    loading: false,
    news: [],
    oneNews: [],
};

const NewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return { ...state, loading: true };
        case FETCH_NEWS_SUCCESS:
            return { ...state, loading: false, news: action.news };
        case FETCH_NEWS_FAILURE:
            return { ...state, loading: false, error: action.error };
        case DELETE_NEWS_REQUEST:
            return { ...state, loading: true };
        case DELETE_NEWS_SUCCESS:
            return { ...state, loading: false };
        case DELETE_NEWS_FAILURE:
            return { ...state, loading: false, error: action.error };
        case POST_NEWS_REQUEST:
            return { ...state, loading: true };
        case POST_NEWS_SUCCESS:
            return { ...state, loading: false };
        case POST_NEWS_FAILURE:
            return { ...state, loading: false, error: action.error };
        case FETCH_ONE_NEWS_REQUEST:
            return { ...state, loading: true };
        case FETCH_ONE_NEWS_SUCCESS:
            return { ...state, loading: false, oneNews: action.one };
        case FETCH_ONE_NEWS_FAILURE:
            return { ...state, loading: false, error: action.error };
        default:
            return state;
    }
};

export default NewsReducer;
