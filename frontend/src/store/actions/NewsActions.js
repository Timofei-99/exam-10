import axiosAPI from "../../axiosAPI";

export const FETCH_NEWS_REQUEST = "FETCH_NEWS_REQUEST";
export const FETCH_NEWS_SUCCESS = "FETCH_NEWS_SUCCESS";
export const FETCH_NEWS_FAILURE = "FETCH_NEWS_FAILURE";

export const FETCH_ONE_NEWS_REQUEST = "FETCH_ONE_NEWS_REQUEST";
export const FETCH_ONE_NEWS_SUCCESS = "FETCH_ONE_NEWS_SUCCESS";
export const FETCH_ONE_NEWS_FAILURE = "FETCH_ONE_NEWS_FAILURE";

export const DELETE_NEWS_REQUEST = "DELETE_NEWS_REQUEST";
export const DELETE_NEWS_SUCCESS = "DELETE_NEWS_SUCCESS";
export const DELETE_NEWS_FAILURE = "DELETE_NEWS_FAILURE";

export const POST_NEWS_REQUEST = "POST_NEWS_REQUEST";
export const POST_NEWS_SUCCESS = "POST_NEWS_SUCCESS";
export const POST_NEWS_FAILURE = "POST_NEWS_FAILURE";

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSucess = (news) => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsFailure = (error) => ({type: FETCH_NEWS_FAILURE, error});

export const fetchOneNewsRequest = () => ({type: FETCH_ONE_NEWS_REQUEST});
export const fetchOneNewsSucess = (one) => ({type: FETCH_ONE_NEWS_SUCCESS, one});
export const fetchOneNewsFailure = (error) => ({type: FETCH_ONE_NEWS_FAILURE, error});

export const deleteNewsRequest = () => ({type: DELETE_NEWS_REQUEST});
export const deleteNewsSucess = () => ({type: DELETE_NEWS_SUCCESS});
export const deleteNewsFailure = (error) => ({type: DELETE_NEWS_FAILURE, error});

export const postNewsRequest = () => ({type: POST_NEWS_REQUEST});
export const postNewsSuccess = () => ({type: POST_NEWS_SUCCESS});
export const postNewsFailure = (error) => ({type: POST_NEWS_FAILURE, error});

export const fetchNews = () => {
    return async (dispatch) => {
        dispatch(fetchNewsRequest());
        try {
            const response = await axiosAPI.get("/news");
            dispatch(fetchNewsSucess(response.data));
        } catch (e) {
            dispatch(fetchNewsFailure(e));
        }
    };
};

export const deleteNews = (id) => {
    return async (dispatch) => {
        dispatch(deleteNewsRequest());
        try {
            await axiosAPI.delete("/news/" + id);
            dispatch(deleteNewsSucess());
            dispatch(fetchNews());
        } catch (e) {
            dispatch(deleteNewsFailure(e));
        }
    };
};

export const postNews = (news) => {
    return async (dispatch) => {
        dispatch(postNewsRequest());
        try {
            await axiosAPI.post("/news", news);
            dispatch(postNewsSuccess());
            dispatch(fetchNews());
        } catch (e) {
            dispatch(postNewsFailure(e));
        }
    };
};

export const fetchOneNews = (id) => {
    return async (dispatch) => {
        dispatch(fetchOneNewsRequest());
        try {
            const response = await axiosAPI.get("/news/" + id);
            dispatch(fetchOneNewsSucess(response.data));
        } catch (e) {
            dispatch(fetchOneNewsFailure(e));
        }
    };
};
