import {Button, Card, CardActions, CardHeader, CardMedia, makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {useDispatch} from "react-redux";
import React from "react";
import {Link} from "react-router-dom";
import {apiURL} from "../../config";
import {deleteNews} from "../../store/actions/NewsActions";

const useStyles = makeStyles({
    card: {
        height: "100%",
        background: "whitesmoke",
    },
    media: {
        height: 100,
        paddingTop: "10.25%",
        width: "30%",
        margin: "10px",
    },
    mediaNonePhoto: {
        display: "none",
    },
    title: {
        background: "#223B8C",
    },
    border: {
        padding: "20px",
        margin: "10px",
    },
});

const Post = (props) => {
    const dispatch = useDispatch();
    const classes = useStyles();

    let cardImage = "Image";

    if (props.image) {
        cardImage = apiURL + "/uploads/" + props.image;
    }

    const removePost = (id) => {
        dispatch(deleteNews(id));
    };

    return (
        <Grid item xs sm={12} md={12} className={classes.border}>
            <Card className={classes.card}>
                <CardHeader className={classes.title}/>
                <Grid item container alignItems="center" justifyContent="space-between">
                    <CardMedia
                        image={cardImage}
                        className={cardImage !== "Image" ? classes.media : classes.mediaNonePhoto}
                        title={props.title}
                    />
                    <Typography variant="h5" style={{padding: "10px 50% 0 10px"}}>
                        {props.title}
                    </Typography>
                </Grid>

                <CardActions>
                    <Grid item container alignItems="center" justifyContent="space-around">
                        <b style={{marginRight: "49%"}}>{props.datetime}</b>
                        <Button component={Link} to={"/news/" + props.id} color="primary">
                            Read Full Post
                        </Button>
                        <Button variant="contained" color="secondary" onClick={() => removePost(props.id)}>
                            Delete
                        </Button>
                    </Grid>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default Post;
