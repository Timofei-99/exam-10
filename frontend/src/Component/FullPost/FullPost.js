import {Button, LinearProgress, makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {apiURL} from "../../config";
import {fetchOneNews} from "../../store/actions/NewsActions";
import {deleteComments, fetchComments} from "../../store/actions/CommentActions";
import AddComment from "../AddComment/AddComment";

const useStyles = makeStyles({
    media: {
        height: "200px",
        width: "300px",
    },
    mediaNonePhoto: {
        display: "none",
    },
    border: {
        margin: "5px",
        border: "3px solid black",
    },
});

const FullPost = ({match}) => {
    const classes = useStyles();

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchOneNews(match.params.id));
        dispatch(fetchComments(match.params.id));
    }, [dispatch, match.params.id]);


    const oneNews = useSelector((state) => state.news.oneNews);
    const comments = useSelector((state) => state.comments.comments);
    const loading = useSelector((state) => state.news.loading);


    let cardImage = "Image";

    if (oneNews.image) {
        cardImage = apiURL + "/uploads/" + oneNews.image;
    }

    const removeComment = (id, news_id) => {
        dispatch(deleteComments(id, news_id));
    };

    return (
        <Grid item container lg={12} md={12} spacing={2} direction="column" className={classes.border}>
            {loading ? <LinearProgress/> : null}
            {oneNews ? <Grid item>
                <Typography variant="h4">{oneNews.title}</Typography>
                <p>{oneNews.publishDate}</p>
                <p>{oneNews.content}</p>
                <img
                    src={cardImage}
                    className={cardImage !== "Image" ? classes.media : classes.mediaNonePhoto}
                    alt="News"
                />
            </Grid> : null}
            <Grid item className={classes.border}>
                <Typography variant="h4">Comments</Typography>
                {comments.map((comment) => {
                    return (
                        <div
                            key={comment.id}
                            style={{
                                border: "1px solid black",
                                padding: "10px",
                                margin: "10px",
                                display: "flex",
                                justifyContent: "space-around",
                            }}
                        >
                            <h3>{comment.author}</h3>
                            <p>{comment.comment}</p>
                            <Button color="secondary" onClick={() => removeComment(comment.id, match.params.id)}>
                                Delete
                            </Button>
                        </div>
                    );
                })}
            </Grid>
            <Grid item>
                <Typography variant="h4">Add Comments</Typography>
                <AddComment id={match.params.id}/>
            </Grid>
        </Grid>
    );
};

export default FullPost;
