import {Button, Grid, LinearProgress, TextField} from "@material-ui/core";
import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {postComments} from "../../store/actions/CommentActions";
import {Link} from "react-router-dom";

const AddComment = (props) => {
    const dispatch = useDispatch();

    const [comment, setComment] = useState({
        news_id: props.id,
        author: "",
        comment: "",
    });

    const loading = useSelector((state) => state.comments.loading);

    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setComment((PrevState) => ({
            ...PrevState,
            [name]: value,
        }));
    };

    const submitForm = (e) => {
        e.preventDefault();

        dispatch(postComments(comment, props.id));

        setComment({
            news_id: props.id,
            author: "",
            comment: "",
        });
    };

    return (
        <form onSubmit={submitForm}>
            {loading ? <LinearProgress color="secondary"/> : null}

            <Grid container spacing={1} direction="column">
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Author"
                        name="author"
                        value={comment.author}
                        onChange={inputChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        fullWidth
                        variant="outlined"
                        label="Comment"
                        name="comment"
                        value={comment.comment}
                        onChange={inputChangeHandler}
                        required
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained" style={{margin: "10px"}}>
                        ADD
                    </Button>
                    <Button color="primary" variant="contained" component={Link} to="/">
                        Go back to the main page
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default AddComment;
