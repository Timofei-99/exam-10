const express = require('express');
const cors = require('cors');
const news = require('./app/news')
const comments = require('./app/comments');
const mysqlDb = require('./mysqlDb');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;
app.use('/news', news);
app.use('/comments', comments);

mysqlDb.connect().catch(e => console.log(e));

app.listen(port, () => {
    console.log(`We are on port ${port}`);
});