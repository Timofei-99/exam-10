drop database if exists exam_10;
create database if not exists exam_10;

use exam_10;

create table if not exists news (
id int auto_increment not null primary key ,
title varchar(255) not null,
content text not null ,
image varchar(255) null,
publishDate datetime default CURRENT_TIMESTAMP not null
);

create table if not exists comments (
    id int auto_increment not null primary key ,
    news_id int not null ,
    author varchar(255) not null ,
    comment text not null ,
    constraint comments_news_id_fk
    foreign key (news_id)
    references news (id)
    on update cascade
    on delete cascade
);

insert into news ( title, content)
values ('World news!', 'Last new of crypto world!'),
       ('Sport News', 'Last news of Kyrgyz Republic sport'),
       ('News of programming', 'New things in Java Script!');

insert into comments (news_id, author, comment)
values (2, 'Tima', 'It was great week in sport life!'),
       (1, 'Ilon Mask', 'Dogecoin gonna grow up more high!'),
       (3, 'Mark Zuckerberg', 'Facebook gonna change name in think week');

