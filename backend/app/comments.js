const express = require('express');
const mysqlDb = require('../mysqlDb');



const router = express.Router();

router.get('/', async (req, res) => {
    try {
        if (req.query.news_id) {
            const [commentById] = await mysqlDb.getConnection().query(
                'SELECT * FROM ?? where news_id = ?',
                ['comments', req.query.news_id]
            );
            return res.send(commentById);
        }
        const [comments] = await mysqlDb.getConnection().query(
            'SELECT * from ??',
            ['comments']
        );
        res.send(comments);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/', async (req, res) => {
    try {
        if (req.body.comment) {
            if (!req.body.author) {
                req.body.author = 'Anonymous';
            }
            const newComment = await mysqlDb.getConnection().query(
                'INSERT INTO ?? (news_id, author, comment) values (?, ?, ?)',
                ['comments', req.body.news_id, req.body.author, req.body.comment]
            )
            res.send({id: newComment[0].insertId, ...req.body});
        }
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'DELETE FROM ?? where id = ?',
            ['comments', req.params.id]
        );
        res.send('Remove was completed');
    } catch (e) {
        res.status(400).send(e);
    }
})


module.exports = router;