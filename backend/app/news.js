const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const mysqlDb = require('../mysqlDb');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename(req, file, cb) {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});


router.get('/', async (req, res) => {
    try {
        const [news] = await mysqlDb.getConnection().query(
            'SELECT id, title, image, publishDate FROM ??',
            ['news']
        );
        res.send(news);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const [newsById] = await mysqlDb.getConnection().query(
            'SELECT * FROM news where id = ?',
            [req.params.id]
        );

        if (!newsById[0]) {
            res.send('I dont have this news');
        }
        res.send(newsById[0]);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        if (req.body.title && req.body.content) {
            if (req.file) {
                req.body.image = req.file.filename;
            }

            const sendNews = await mysqlDb.getConnection().query(
                'INSERT INTO ?? (title, content, image) values (?, ?, ?)',
                ['news',
                    req.body.title,
                    req.body.content,
                    req.body.image
                ]
            );
            res.send({id: sendNews[0].insertId, ...req.body});
        }
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete('/:id', async (req, res) => {
    try {
        await mysqlDb.getConnection().query(
            'DELETE FROM ?? where id = ?',
            ['news', req.params.id]
        );
        res.send('Remove was completed!');
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;